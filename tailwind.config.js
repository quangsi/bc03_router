module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}", "./src/**/**/*.{js,jsx,ts,tsx}"],
  mode: "jit",
  important: true,
  theme: {
    extend: {
      colors: {
        ["yellow-theme"]: "#FFEE63",
      },
    },
  },
  plugins: [],
};
