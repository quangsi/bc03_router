import axios from "axios";

const BASE_URL = "https://movienew.cybersoft.edu.vn";
export const movieService = {
  getMovieList: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`,
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCDEkMOgIE7hurVuZyAwMyIsIkhldEhhblN0cmluZyI6IjEwLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NTk4NzIwMDAwMCIsIm5iZiI6MTY0NTgwODQwMCwiZXhwIjoxNjc2MTM0ODAwfQ.eP1tQ1Ucwbf0A94jW8e7zfHEKZS8iYyG0EIiHa9udpw",
      },
    });
  },
  getDetailMovie: (maPhim) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`,
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCDEkMOgIE7hurVuZyAwMyIsIkhldEhhblN0cmluZyI6IjEwLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NTk4NzIwMDAwMCIsIm5iZiI6MTY0NTgwODQwMCwiZXhwIjoxNjc2MTM0ODAwfQ.eP1tQ1Ucwbf0A94jW8e7zfHEKZS8iYyG0EIiHa9udpw",
      },
    });
  },
};
