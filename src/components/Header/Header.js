import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";

export default class extends Component {
  render() {
    return (
      <div>
        <button>
          <NavLink
            activeClassName=" text-blue-500     "
            to="/"
            exact
            className="underline mx-5 text-xl font-medium  "
          >
            Home
          </NavLink>
          <NavLink
            activeClassName=" text-blue-500     "
            to="/about"
            className="underline mx-5 text-xl font-medium  "
          >
            About
          </NavLink>

          <NavLink
            activeClassName=" text-blue-500     "
            to="/detail/478"
            className="underline mx-5 text-xl font-medium  "
          >
            Detail
          </NavLink>

          <NavLink
            activeClassName=" text-blue-500     "
            to="demo-hook"
            className="underline mx-5 text-xl font-medium  "
          >
            DemoHook
          </NavLink>
        </button>
      </div>
    );
  }
}
