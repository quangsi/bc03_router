import React, { Component } from "react";
import { Button, Card } from "antd";
import { NavLink } from "react-router-dom";
import AboutPage from "../../pages/AboutPage";
const { Meta } = Card;

export default class MovieItem extends Component {
  render() {
    let { movie } = this.props;
    console.log("movie: ", movie);
    return (
      <Card
        hoverable
        style={{
          width: 240,
        }}
        cover={
          <img
            alt="example"
            style={{
              width: "100%",
              height: "300px",
              objectFit: "cover",
            }}
            src={movie.hinhAnh}
          />
        }
      >
        <Meta title={movie.tenPhim} description={movie.ngayKhoiChieu} />
        <NavLink to={`/detail/${movie.maPhim}`}>
          <Button type="primary" danger>
            Xem chi tiết
          </Button>
        </NavLink>
      </Card>
    );
  }
}
// {
//     "maPhim": 8849,
//     "tenPhim": "Công tố viên chuyển sinh 1",
//     "biDanh": "cong-to-vien-chuyen-sinh-1",
//     "trailer": "youtube.com/watch?v=H8qti-dUUmU",
//     "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/cong-to-vien-chuyen-sinh_gp01.jpg",
//     "moTa": "Công tố viên chuyển sinh",
//     "maNhom": "GP01",
//     "ngayKhoiChieu": "2022-05-23T18:39:14.173",
//     "danhGia": 9,
//     "hot": false,
//     "dangChieu": true,
//     "sapChieu": false
// }
