import React, { Component } from "react";
import { movieService } from "../services/movieService";

export default class DetailPage extends Component {
  componentDidMount() {
    let maPhim = this.props.match.params.maPhim;
    movieService
      .getDetailMovie(maPhim)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    return <div>DetailPage</div>;
  }
}
// withRouter
