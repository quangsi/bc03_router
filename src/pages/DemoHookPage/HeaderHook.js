import React, { useEffect } from "react";

function HeaderHook({ dataNumber, handlePlusNumber }) {
  let isLogin = true;
  useEffect(() => {
    console.log("HeaderHook didmount - dataNumber chaged");
  }, [dataNumber]);

  useEffect(() => {
    console.log("HeaderHook didmount - isLogin chaged");
  }, [isLogin]);

  console.log("HeaderHook render");
  return (
    <div className="h-max p-5 w-full bg-yellow-200 text-center">
      <span>HeaderHook</span>
      <p className=" texl-2xl ">Number - {dataNumber}</p>

      <button
        onClick={handlePlusNumber}
        className=" rounded px-5 py-2 bg-indigo-300"
      >
        Plush number in header
      </button>
    </div>
  );
}
export default React.memo(HeaderHook);
// memo ~ PureComponent ~ shallow compare
