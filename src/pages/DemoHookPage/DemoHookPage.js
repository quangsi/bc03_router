import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import HeaderHook from "./HeaderHook";

export default function DemoHookPage() {
  const [number, setNumber] = useState(10);
  const [like, setLike] = useState(10);
  const [numbers, setNumbers] = useState([1, 2, 3, 4]);
  let inputRef = useRef();
  useEffect(() => {
    inputRef.current.focus();
  }, []);
  // useMemo ghi nhớ kết quả tính toán
  let totalNumber = useMemo(() => {
    return numbers.reduce((prev, total) => {
      console.log("reduce render");
      return total + prev;
    }, 0);
  }, [numbers]);
  console.log("totalNumber: ", totalNumber);
  const handlePlusNumber = () => {
    setNumber(number + 1);
  };
  const handlePlusLike = () => {
    setLike(like + 1);
  };
  //useCallback  xứ lý function ( tryền function vào props  )

  const handlePlusUseCallBack = useCallback(() => {
    {
      setNumber(number + 1);
    }
  }, [number]);

  return (
    <div className=" h-max space-y-5 text-center text-3xl">
      <HeaderHook
        dataNumber={number}
        handlePlusNumber={handlePlusUseCallBack}
      />

      <input ref={inputRef} type="text" className=" border-2 border-red-500" />
      <div>
        {number} -{" "}
        <button
          className=" rounded px-5 py-2 bg-slate-300"
          onClick={handlePlusNumber}
        >
          Plus number
        </button>
      </div>
      <div className="mt-20">
        {like} -{" "}
        <button
          className=" rounded px-5 py-2 bg-orange-500 text-white"
          onClick={handlePlusLike}
        >
          Plus like
        </button>
      </div>
    </div>
  );
}
