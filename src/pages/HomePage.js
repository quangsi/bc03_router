import { Col, Row } from "antd";
import React, { Component } from "react";
import MovieItem from "../components/MovieItem/MovieItem";
import { movieService } from "../services/movieService";

export default class HomePage extends Component {
  state = {
    movieList: [],
  };
  componentDidMount() {
    movieService
      .getMovieList()
      .then((res) => {
        console.log("res: ", res);
        this.setState({ movieList: res.data.content });
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  }
  render() {
    return (
      <div style={{ padding: "10px 100px" }}>
        <Row>
          {this.state.movieList.map((movie, index) => {
            return (
              <Col span={6}>
                <MovieItem movie={movie} key={index} />
              </Col>
            );
          })}
        </Row>
      </div>
    );
  }
}
