import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import HomePage from "./pages/HomePage";
import { Route } from "react-router-dom";
import AboutPage from "./pages/AboutPage";
import DetailPage from "./pages/DetailPage";
import Header from "./components/Header/Header";
import NotFoundPage from "./pages/404Page";
import DemoHookPage from "./pages/DemoHookPage/DemoHookPage";
import { Switch } from "react-router-dom";

function App() {
  return (
    <div className=" ">
      <h2 className="text-yellow-theme bg-yellow-theme  border-red-500">
        Hello
      </h2>
      <BrowserRouter>
        <Header />
        {/* <Route path="/home" component={HomePage} /> */}
        <Switch>
          <Route
            path={"/"}
            // exact={true}
            exact
            render={() => {
              return <HomePage />;
            }}
          />

          <Route path="/about" component={AboutPage} />
          <Route path="/detail/:maPhim" component={DetailPage} />

          <Route path="/demo-hook" component={DemoHookPage} />
          <Route path="*" component={NotFoundPage} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
